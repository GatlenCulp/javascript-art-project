// https://www.w3schools.com/graphics/canvas_drawing.asp
// Initialize Canvas
const canvas = document.getElementById("the-art");
const ctx = canvas.getContext("2d");
var canvasWidth;
var canvasHeight;

var paused = false;

var i = 0;
var i_increasing = true;
var trail_length = 200;

function init() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvasWidth = canvas.width;
    canvasHeight = canvas.height;
    ctx.fillStyle = "#FFFFFF";
    ctx.strokeStyle = "#FFFFFF";
}

function drawSquares(x, y) {
    // ctx.clearRect(0, 0, canvas.width, canvas.height);
    // Select trail length
    if (i >= trail_length) i_increasing = false;
    else if (i <= 0) i_increasing = true;
    if (i_increasing) i += 1;
    else i -= 2;

    squareSide = 300;
    for (k = 1; k > 0; k -= 0.25) {
        windowCenterToSquareSide = k*squareSide/2;
        // Top Left
        ctx.moveTo(x - windowCenterToSquareSide, y - windowCenterToSquareSide);
        ctx.lineTo(x - windowCenterToSquareSide - i, y - windowCenterToSquareSide);
        ctx.moveTo(x - windowCenterToSquareSide, y - windowCenterToSquareSide);
        // Top Right
        ctx.lineTo(x + windowCenterToSquareSide, y - windowCenterToSquareSide);
        ctx.lineTo(x + windowCenterToSquareSide, y - windowCenterToSquareSide - i);
        ctx.moveTo(x + windowCenterToSquareSide, y - windowCenterToSquareSide);
        // Bottom Right
        ctx.lineTo(x + windowCenterToSquareSide, y + windowCenterToSquareSide);
        ctx.lineTo(x + windowCenterToSquareSide + i, y + windowCenterToSquareSide);
        ctx.moveTo(x + windowCenterToSquareSide, y + windowCenterToSquareSide);
        // Bottom Left
        ctx.lineTo(x - windowCenterToSquareSide, y + windowCenterToSquareSide);
        ctx.lineTo(x - windowCenterToSquareSide, y + windowCenterToSquareSide + i);
        ctx.moveTo(x - windowCenterToSquareSide, y + windowCenterToSquareSide);
        // Back to Top Left
        ctx.lineTo(x - windowCenterToSquareSide, y - windowCenterToSquareSide);
    }
    ctx.stroke();
}

function drawBorder() {
    // Cool Top effect
    ctx.fillStyle = "#FFFFFF";
    ctx.strokeStyle = "#FFFFFF";
    
    // Changing x interval sampling results in some interesting beat patterns
    var x_interval = 2;
    
    ctx.moveTo(0,0);
    length = 0.1 * canvasHeight;

    for (var x = 0; x < 2000; x += x_interval) {
        ctx.lineTo(4*x, length*Math.sin(x))
    }
    
    ctx.moveTo(0,canvasHeight);
    for (var x = 0; x < 2000; x += x_interval) {
        ctx.lineTo(4*x, length*Math.sin(x) + canvasHeight)
    }

    ctx.stroke();
}

function randBool() {
    return Boolean(Math.round(Math.random()));
}

function drawTilingLines(x, y) {
    // Creates a vertical stack of lines
    var box_size = 10;
    var chain_length = 80;
    var bottom_offset = 15;

    for (var delta_y = 20; delta_y <= chain_length; delta_y += box_size) {
        bottom_to_top = randBool();
        if (bottom_to_top) {
            // Below Y
            ctx.moveTo(x - box_size/2, y +       delta_y + bottom_offset             ); //console.log("move to", x - box_size/2, y_pos);
            ctx.lineTo(x + box_size/2, y +       delta_y + box_size + bottom_offset  ); //console.log("line to", x + box_size/2, y_pos+box_size);
            // Above Y
            ctx.moveTo(x - box_size/2, y + -1 * (delta_y)            );
            ctx.lineTo(x + box_size/2, y + -1 * (delta_y + box_size) );
        }
        else {
            // Below Y
            ctx.moveTo(x + box_size/2, y +       delta_y + bottom_offset            ); //console.log("move to", x - box_size/2, y_pos);
            ctx.lineTo(x - box_size/2, y +       delta_y + box_size + bottom_offset  ); //console.log("line to", x + box_size/2, y_pos+box_size);
            // Above Y
            ctx.moveTo(x + box_size/2, y + -1 * (delta_y)            );
            ctx.lineTo(x - box_size/2, y + -1 * (delta_y + box_size) );
        }
    }
    ctx.stroke();
}

function update() {
    if (!paused) {
        init();
        drawSquares(canvasWidth/2, canvasHeight/2);
        // if (mouse_down) drawSquares(mouse_pos.x, mouse_pos.y);
        drawBorder();
        drawTilingLines(mouse_pos.x, mouse_pos.y);
    }
}

// // var time = new Date();
// // console.log(time.getMilliseconds);

var mouse_pos = {x: 0, y: 0};
canvas.addEventListener("mousemove", (evt) => {
    mouse_pos.x = evt.clientX;
    mouse_pos.y = evt.clientY;
});

mouse_down = false;
canvas.addEventListener("mousedown", (evt) => {
    mouse_down = true;
});
canvas.addEventListener("mouseup", (evt) => {
    mouse_down = false;
});
canvas.addEventListener("click", (evt) => {
    paused = !paused;
});

window.setInterval(update, 10);